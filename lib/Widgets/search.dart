import 'package:flutter/material.dart';

class SearchContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(
          horizontal: 5
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
            30
        ),
      ),
      child: TextField(
        decoration: InputDecoration(
            enabledBorder: InputBorder.none,
            hintText: 'Search...',
            hintStyle: TextStyle(
                color: Colors.grey.shade400
            ),
            contentPadding: EdgeInsets.only(
                left: 20,
                right: 25,
                top: 15
            ),
            suffixIcon: Icon(
              Icons.search,
              color: Colors.grey.shade400,
            )
        ),
      ),
    );
  }
}
