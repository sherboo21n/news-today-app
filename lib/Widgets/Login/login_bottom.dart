import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/home.dart';
import 'package:news_today_app/Screens/login.dart';

class LoginBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(
          left: 10
      ),
      child: RaisedButton(
        color: Colors.grey.shade600,
        child:  Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 15
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 130,
                right: 130
            ),
            child: Text(
              'Sign In',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16
              ),
            ),
          ),
        ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) =>HomeScreen()));
        },
      ),
    );
  }
}
