import 'package:flutter/material.dart';

import 'login_bottom.dart';

class LoginContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: 20
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                hintText: 'E-mail',
                hintStyle: TextStyle(
                  color: Colors.grey.shade600,
                ),
                suffixIcon: Icon(
                  Icons.mail_outline,
                  color: Colors.grey.shade400,
                )
            ),
          ),
          SizedBox(
            height: 30,
          ),
          TextField(
            decoration: InputDecoration(
                hintText: 'Password',
                hintStyle: TextStyle(
                  color: Colors.grey.shade600,
                ),
                suffixIcon: Icon(
                  Icons.lock_outline,
                  color: Colors.grey.shade400,
                )
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Text(
            'Forget password?',
            style: TextStyle(
                color: Colors.blue.shade800
            ),
          ),
          SizedBox(
            height: 25,
          ),
         LoginBottom(),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Dont have an account? ',
                style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 16
                ),
              ),
              Text(
                'Sign Up',
                style: TextStyle(
                    color: Colors.purple.shade500,
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
