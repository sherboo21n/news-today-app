import 'package:flutter/material.dart';

class CategoryImage extends StatelessWidget {

  final String image;
  CategoryImage({this.image});

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: 100,
      margin: EdgeInsets.only(
          left: 10,
          right: 10,
          bottom: 10,
          top: 10
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(
              10
          ),
          image: DecorationImage(
              image: AssetImage(
                  image
              ),
              fit: BoxFit.cover
          )
      ),
    );
  }
}
