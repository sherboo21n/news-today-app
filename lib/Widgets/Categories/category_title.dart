import 'package:flutter/material.dart';

class CategoryTitle extends StatelessWidget {

  final String title;
  CategoryTitle({this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: EdgeInsets.only(
          left: 10,
          right: 10,
          bottom: 10,
          top: 10
      ),
      decoration: BoxDecoration(
        color: Colors.black38,
        borderRadius: BorderRadius.circular(
            10
        ),
      ),
      child: Center(
        child: Text(
          title,
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
}
