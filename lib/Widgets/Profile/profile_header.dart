import 'package:flutter/material.dart';

class ProfileHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage(
              'images/sherboo.jpg'
          ),
          radius: 40,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'S H E R B O O',
          style: TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.bold
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          'Sherboo21n@gmail.com',
          style: TextStyle(
            color: Colors.black,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: 150
          ),
          child: RaisedButton(
            color: Colors.grey.shade500,
            shape: StadiumBorder(),
            onPressed: (){},
            child: Center(
              child: Text(
                'Log Out',
                style: TextStyle(
                    color: Colors.white
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
