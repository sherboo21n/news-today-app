import 'package:flutter/material.dart';

class WelcomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.symmetric(
          horizontal: 20
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Welcome to ',
                style: TextStyle(
                    color: Colors.grey.shade500,
                    fontWeight: FontWeight.bold,
                    fontSize: 26
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'News',
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 26,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  Text(
                    'Today',
                    style: TextStyle(
                        color: Colors.yellow.shade500,
                        fontSize: 26,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Start to continue',
            style: TextStyle(
                color: Colors.grey.shade500,
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ],
      ),
    );
  }
}
