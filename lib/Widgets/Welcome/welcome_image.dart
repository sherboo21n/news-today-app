import 'package:flutter/material.dart';


class WelcomeImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: 60,
          left: 110,
          right: 110
      ),
      child: Image.asset(
        'images/nt5.jpg',
        fit: BoxFit.cover,
        height: 150,
      ),
    );
  }
}
