import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/login.dart';

class WelcomeBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: 170
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            color: Theme.of(context).primaryColor,
            child:  Padding(
              padding: const EdgeInsets.symmetric(
                  vertical: 20
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 100,
                    right: 100
                ),
                child: Text(
                  'Lets Get Started',
                  style: TextStyle(
                      color: Colors.white70,
                      fontSize: 16
                  ),
                ),
              ),
            ),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>Login()));
            },
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Have you been registered',
            style: TextStyle(
                color: Colors.grey.shade800,
                fontSize: 18
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
              'Continue with Email >>',
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 18
              ),
            ),
        ],
      ),
    );
  }
}
