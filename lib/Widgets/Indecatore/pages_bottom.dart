import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/welcome.dart';

class PagesBottom extends StatelessWidget {

  final String name;
  PagesBottom({this.name});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 40,
        top: 110,
        right: 40,
      ),
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        child:  Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 15
          ),
          child: Text(
            name,
            style: TextStyle(
                color: Colors.white70,
                fontSize: 16
            ),
          ),
        ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) =>Welcome()));
        },
      ),
    );
  }
}
