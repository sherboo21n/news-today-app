import 'package:flutter/material.dart';

class PagesImage extends StatelessWidget {

  final String image;
  PagesImage({this.image});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: 45
      ),
      child: Image.asset(
        image,
        fit: BoxFit.cover,
        height: 200,
      ),
    );
  }
}
