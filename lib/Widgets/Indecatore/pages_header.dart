import 'package:flutter/material.dart';

class PagesHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: 30
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'NEWS',
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 30,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            'TODAY',
            style: TextStyle(
                color: Colors.yellow.shade500,
                fontSize: 30,
                fontWeight: FontWeight.bold
            ),
          )
        ],
      ),
    );
  }
}
