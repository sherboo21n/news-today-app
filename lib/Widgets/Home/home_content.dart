import 'package:flutter/material.dart';

class HomeContent extends StatelessWidget {

  final String title;
  HomeContent({this.title});

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(
          left: 20
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 35,
            width: 6,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                    10
                ),
                color: Theme.of(context).primaryColor
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 5
            ),
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
              ),
            ),
          ),
        ],
      ),
    );
  }
}
