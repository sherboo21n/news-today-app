import 'package:flutter/material.dart';

class HomeContImage extends StatelessWidget {

  final String image ;
  final double right;
  HomeContImage({this.image,this.right});
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 20,
      bottom: 20,
      right: right,
      child: Container(
        height: 210,
        width: 140,
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(
                15
            ),
            image: DecorationImage(
                image: NetworkImage(
                    image
                ),
                fit: BoxFit.cover
            )
        ),
      ),
    );
  }
}
