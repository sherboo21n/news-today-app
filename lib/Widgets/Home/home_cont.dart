import 'dart:math';

import 'package:flutter/material.dart';
import 'package:news_today_app/Models/hot_news.dart';
import 'package:news_today_app/Models/news.dart';
import 'package:news_today_app/Screens/article_view.dart';

import 'home_cont_content.dart';
import 'home_cont_image.dart';

class HomeCont extends StatelessWidget {

  final List title;
  final double right , left , rr;
  HomeCont({this.title,this.right,this.left,this.rr});

  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
          children: title.map((e) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
             Stack(
               children: <Widget>[
                 InkWell(
                   onTap: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context) =>ArticleView(
                       blogUrl: e.url,
                     )));
                   },
                   child: Container(
                     height: 200,
                     margin: EdgeInsets.symmetric(
                         horizontal: 10,
                         vertical: 10
                     ),
                     decoration: BoxDecoration(
                         color: Colors.white,
                         borderRadius: BorderRadius.circular(
                             10
                         )
                     ),
                   ),
                 ),
                 HomeContImage(
                   image: e.image,
                   right: right,
                 ),
                HomeContContent(
                  title: e.title,
                  desc: e.desc,
                  time: e.time,
                  url: e.url,
                  left: left,
                  rr: rr,
                ),
               ],
             )
            ],
          )
          ).toList()
      ),
    );
  }
}
