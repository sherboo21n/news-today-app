import 'dart:math';

import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/article_view.dart';

class HomeContContent extends StatelessWidget {

  final String title , desc , url , time;
  final double left , rr;
  HomeContContent({this.desc,this.rr,this.title, this.time ,this.url,this.left});

  @override
  Widget build(BuildContext context) {
    return  Positioned(
      top: 20,
      left: left,
      right: rr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>ArticleView(
                blogUrl: url,
              )));
            },
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            desc,
            style: TextStyle(
                color: Colors.grey.shade600,
              fontSize: 10
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.timer,
                color: Theme.of(context).primaryColor,
                size: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 3
                ),
                child: Text(
                  time,
                  style: TextStyle(
                    color: Colors.grey.shade500,
                    fontSize: 12
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
