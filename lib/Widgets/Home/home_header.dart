import 'package:flutter/material.dart';

class HomeHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(
          left: 15,
          top: 10
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: Theme.of(context).primaryColor,
                        width: 1.5
                    )
                ),
              ),
              Positioned(
                left: 5,
                right: 5,
                top: 5,
                bottom: 5,
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      'images/sherboo.jpg'
                  ),
                  radius: 50,
                )
              ),
            ],
          ),
          SizedBox(
            width: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 15
            ),
            child: Text(
              'Welcome, S H E R B O O',
              style: TextStyle(
                color: Colors.grey.shade900,
                fontSize: 17,
              ),
            ),
          ),
        ],
      ),

    );
  }
}
