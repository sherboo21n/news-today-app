import 'package:flutter/material.dart';

class HomeImageContent extends StatelessWidget {

  final String title;
  HomeImageContent({this.title});

  @override
  Widget build(BuildContext context) {
    return  Positioned(
      bottom: 5,
      left: 20,
      right: 20,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                right: 30
            ),
            child: Text(
              title + ' Andalkan Realme 7 Series - '
                  'Uzone Indonesia',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Uzone id',
                style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 12
                ),
              ),
              Text(
                'about an hour ago',
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 12
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
