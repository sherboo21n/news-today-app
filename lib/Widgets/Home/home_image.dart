import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_today_app/Models/news.dart';

import 'home_image_content.dart';

class HomeImage extends StatelessWidget {

   final double width ;
   HomeImage({this.width});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: news.map((e) => Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Stack(
                children: <Widget>[
                  Container(
                    height: 200,
                    width: width,
                    margin: EdgeInsets.symmetric(
                        horizontal: 10
                    ),
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(
                            10
                        ),
                        image: DecorationImage(
                            image: NetworkImage(
                                e.image
                            ),
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                  Container(
                    height: 200,
                    width: width,
                    margin: EdgeInsets.symmetric(
                        horizontal: 10
                    ),
                    decoration: BoxDecoration(
                        color: Colors.black26,
                        borderRadius: BorderRadius.circular(
                            10
                        ),
                    ),
                  ),
                 HomeImageContent(
                   title: e.title,
                 ),
                ],
              )
            ],
          )
          ).toList()
      ),
    );
  }
}
