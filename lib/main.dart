import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/home.dart';
import 'package:news_today_app/Screens/login.dart';
import 'package:news_today_app/Screens/welcome.dart';
import 'Screens/pages.dart';
import 'Screens/splach.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.purple.shade700
      ),
      home: Splach(),
    );
  }
}
