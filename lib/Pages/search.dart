import 'package:flutter/material.dart';
import 'package:news_today_app/Models/hot_news.dart';
import 'package:news_today_app/Models/news.dart';
import 'package:news_today_app/Widgets/Home/home_cont.dart';
import 'package:news_today_app/Widgets/search.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          SearchContent(),
          SizedBox(
            height: 10,
          ),
          HomeCont(
            title: hotNews,
            right: 250,
            left: 200,
            rr: 30,
          ),
        ],
      ),
    );
  }
}
