import 'package:flutter/material.dart';
import 'package:news_today_app/Widgets/Profile/profile_header.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  List<_cont> _conta = [
    _cont(
      icons: Icons.star_border,
      name:  'Contact Us',
      desc:   'Rate this app on App Store & Google Play',
    ),
    _cont(
      icons:  Icons.toll,
      name:  'About App',
      desc:  'App details',
    ),
    _cont(
      icons:  Icons.arrow_back_ios,
      name:   'Full Source Code',
      desc:   'Get source code of app',
    ),
    _cont(
      icons: Icons.slideshow,
      name:   'Channel Youtube',
      desc:   'Watch the Demo app on Youtube'
    ),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
        ProfileHeader(),
          SizedBox(
            height: 20,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(
              top: 40
            ),
            color: Colors.white,
            child: Column(
              children: _conta.map((e) => Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(
                          5
                        )
                      ),
                      child: Center(
                        child: Icon(
                          e.icons,
                          size: 20,
                            color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          e.name,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                          ),
                        ),
                        SizedBox(
                          height: 2.5,
                        ),
                        Text(
                          e.desc,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
              ).toList()
            )
          )
        ],
      ),
    );
  }
}

class _cont{
 final IconData icons;
 final String name;
  final String desc;

  _cont({this.icons,this.desc,this.name});
}
