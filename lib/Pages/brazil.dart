import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:news_today_app/Classes/news.dart';
import 'package:news_today_app/Models/hot_news.dart';
import 'package:news_today_app/Models/news.dart';
import 'package:news_today_app/Responses/get_hot_news.dart';
import 'package:news_today_app/Responses/get_hot_news2.dart';
import 'package:news_today_app/Responses/get_hot_news3.dart';
import 'package:news_today_app/Widgets/Home/home_cont.dart';
import 'package:news_today_app/Widgets/Home/home_content.dart';
import 'package:news_today_app/Widgets/Home/home_image.dart';

class Brazil extends StatefulWidget {
  @override
  _BrazilState createState() => _BrazilState();
}

class _BrazilState extends State<Brazil> {

  bool _isLoading = true;

  getNews() async {
    News newsClass = News();
    await newsClass.getNews();
    news = newsClass.news2;
    setState(() {
      _isLoading = !_isLoading;
    });
  }
  getHotNews3() async {
    HotNews3 newsClass = HotNews3();
    await newsClass.getHotNews();
    hotNews = newsClass.news5;
  }

  @override
  void initState() {
    getNews();
    getHotNews3();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: <Widget>[
          HomeContent(
            title: 'Featured',
          ),
          SizedBox(
            height: 20,
          ),
          _isLoading? Center(
              child: CircularProgressIndicator(
                backgroundColor: Theme.of(context).primaryColor,
              )
          ):
          HomeImage(
            width: 340,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              HomeContent(
                title: 'Hot News',
              ),
              InkWell(
                onTap: (){
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 12.5,
                      right: 12.5
                  ),
                  child: Text(
                    'Show all',
                    style: TextStyle(
                        color: Theme.of(context).primaryColor
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          _isLoading? Center(
              child: CircularProgressIndicator(
                backgroundColor: Theme.of(context).primaryColor,
              )
          ):
          HomeCont(
            title: hotNews,
            right: 20,
            left: 30,
            rr: 180,
          ),
        ],
      ),
    );
  }
}
class News {
  List<ArticleModel> news2 = [];

  Future<void> getNews() async {
    String url = 'http://newsapi.org/v2/top-headlines?country=br&category=business&apiKey=c6409c8598bf44398d2c8b59d2714f35';
    var response = await http.get(url);
    var jsonData = json.decode(response.body);
    if (jsonData['status'] == 'ok') {
      jsonData['articles'].forEach((data) {
        if (data['author'] != null && ['description'] != null) {
          ArticleModel articleModel = ArticleModel(
              title: data['title'],
              desc: data['description'],
              auther: data['author'],
              url: data['url'],
              image: data['urlToImage'],
              content: data['content'],
              time: data["publishedAt"]
          );
          news2.add(articleModel);
        }
      });
    }
  }
}