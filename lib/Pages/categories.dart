import 'package:flutter/material.dart';
import 'package:news_today_app/Classes/category.dart';
import 'package:news_today_app/Models/category.dart';
import 'package:news_today_app/Models/news.dart';
import 'package:news_today_app/Screens/category.dart';
import 'package:news_today_app/Screens/category.dart';
import 'package:news_today_app/Widgets/Categories/category_image.dart';
import 'package:news_today_app/Widgets/Categories/category_title.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {

  List<Category> categories2 = new List<Category>();

  @override
  void initState() {
    categories2  = category;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: category.map((e) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>CategoriesScreen(
                        title: e.title,
                      )));
                    },
                    child: Stack(
                      children: <Widget>[
                       CategoryImage(
                         image: e.image,
                       ),
                        CategoryTitle(
                          title: e.title,
                        ),
                      ],
                    ),
                  )
                ],
              )
              ).toList()
            ),
          )
        ],
      ),
    );
  }
}



