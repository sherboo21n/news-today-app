import 'package:news_today_app/Classes/category.dart';

List<Category> category = [
  Category(
    image: 'images/b.jpeg',
    title: 'Business'
  ),
  Category(
    image: 'images/e.png',
    title: 'Entertainment'
  ),
  Category(
    image: 'images/h.jpg',
    title: 'Health'
  ), Category(
    image: 'images/s.jpg',
    title: 'Science'
  ), Category(
    image: 'images/sp.jpg',
    title: 'Sports'
  ), Category(
    image: 'images/t.jpg',
    title: 'Technology'
  ), Category(
    image: 'images/f.png',
    title: 'Arts'
  ),

];