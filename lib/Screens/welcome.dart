import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_today_app/Classes/pages.dart';
import 'package:news_today_app/Widgets/Welcome/welcome_bottom.dart';
import 'package:news_today_app/Widgets/Welcome/welcome_content.dart';
import 'package:news_today_app/Widgets/Welcome/welcome_image.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/news_today_app/lib/Widgets/Indecatore/pages_bottom.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          WelcomeImage(),
          SizedBox(
            height: 50,
          ),
         WelcomeContent(),
         WelcomeBottom()
        ],
      )
    );
  }
}
