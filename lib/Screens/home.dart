import 'package:flutter/material.dart';
import 'package:news_today_app/Pages/categories.dart';
import 'package:news_today_app/Pages/home.dart';
import 'package:news_today_app/Pages/profile.dart';
import 'package:news_today_app/Pages/search.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int _currentIndex = 0;
  void _onChange(int val){
    setState(() {
      _currentIndex = val;
    });
  }

  final tabs = [
    Home(),
   Search(),
    Categories(),
    Profile()
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title:  Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'NEWS',
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              'TODAY',
              style: TextStyle(
                  color: Colors.yellow.shade700,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
              ),
            )
          ],
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
        bottomNavigationBar:  BottomNavigationBar(
          elevation: 0.0,
          type: BottomNavigationBarType.fixed,
          iconSize: 25,
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey.shade600,
          currentIndex: _currentIndex,
          onTap: _onChange,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.home
              ),
              title: Text(
                  'Home'
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.search
              ),
              title: Text(
                  'Search'
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.zoom_out_map
              ),
              title: Text(
                  'Category'
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.person_outline
              ),
              title: Text(
                  'Me'
              ),
            ),
          ],
        ),
        body: tabs[_currentIndex]


    );
  }


}

