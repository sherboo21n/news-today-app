import 'package:flutter/material.dart';
import 'package:news_today_app/Models/pages.dart';
import 'package:news_today_app/Widgets/Indecatore/pages_bottom.dart';
import 'package:news_today_app/Widgets/Indecatore/pages_content.dart';
import 'package:news_today_app/Widgets/Indecatore/pages_header.dart';
import 'package:news_today_app/Widgets/Indecatore/pages_image.dart';
import 'package:page_view_indicator/page_view_indicator.dart';



class PageIndicatore extends StatefulWidget {
  @override
  _PageIndicatoreState createState() => _PageIndicatoreState();
}

class _PageIndicatoreState extends State<PageIndicatore> {

  ValueNotifier<int> _pageviewnotifier = ValueNotifier(0);
  int _selectedindex = 0;

  @override
  Widget build(BuildContext context) {
    pages ;
    return Stack(
      children: <Widget>[
        Scaffold(
          body: PageView.builder(
              onPageChanged: (Index ){
                setState(() {
                  _pageviewnotifier.value = Index;
                });
              },
              itemCount: pages.length,
              itemBuilder: (context , index) =>ListView(
                shrinkWrap: true,
                children: <Widget>[
                  PagesHeader(),
                  SizedBox(
                    height: 70,
                  ),
                PagesImage(
                  image: pages[index].image,
                ),
                  SizedBox(
                    height: 70,
                  ),
                  PagesContent(
                    title: pages[index].title,
                    desc: pages[index].descrebtion,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  PagesBottom(
                      name: 'Continue',
                  )
                ],)

          ),
        ),
        Transform.translate(
          offset: Offset(
              0,
              450
          ),
          child: Align(
            alignment: Alignment.center,
            child: _displayPageIndicators(
                pages.length
            ),
          ),
        ),

      ],
    );
  }
  Widget _displayPageIndicators(int length){
    return  Padding(
      padding: EdgeInsets.only(
          bottom: 570
      ),
      child: PageViewIndicator(
        alignment:MainAxisAlignment.center,
        pageIndexNotifier: _pageviewnotifier,
        length: length,
        normalBuilder: (animationController, index) =>  Container(
          width:  _pageviewnotifier.value == index ?
          22 : 7,
          height:  _pageviewnotifier.value == index ?
          7 : 7,
          decoration: BoxDecoration(
              color:  _pageviewnotifier.value == index ?
              Theme.of(context).primaryColor : Colors.grey.shade600,
              borderRadius: BorderRadius.circular(
                  _pageviewnotifier.value == index ?
                  10 : 7
              )
          ),
        ),
        highlightedBuilder: (animationController, index) => ScaleTransition(
          scale: CurvedAnimation(
            parent: animationController,
            curve: Curves.ease,
          ),
        ),
      ),
    );
  }
}
