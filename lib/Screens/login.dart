import 'package:flutter/material.dart';
import 'package:news_today_app/Widgets/Indecatore/pages_bottom.dart';
import 'package:news_today_app/Widgets/Login/login_content.dart';
import 'package:news_today_app/Widgets/Login/login_header.dart';
import 'package:news_today_app/Widgets/Welcome/welcome_bottom.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading:   IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey.shade600,
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
        LoginHeader(),
          SizedBox(
            height: 120,
          ),
          LoginContent()

        ],
      ),
    );
  }
}
