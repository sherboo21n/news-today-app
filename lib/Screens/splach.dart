import 'dart:async';

import 'package:flutter/material.dart';
import 'package:news_today_app/Screens/pages.dart';

class Splach extends StatefulWidget {
  @override
  _SplachState createState() => _SplachState();
}

class _SplachState extends State<Splach> {

  @override
  void initState() {
    Timer(
        Duration(seconds: 5) , (){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>PageIndicatore()));
    }
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child:  Image.asset(
            'images/nt.jpg',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        )
    );
  }
}