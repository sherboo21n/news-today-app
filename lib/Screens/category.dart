import 'package:flutter/material.dart';
import 'package:news_today_app/Classes/news.dart';
import 'package:news_today_app/Models/hot_news.dart';
import 'package:news_today_app/Models/news.dart';
import 'package:news_today_app/Pages/home.dart';
import 'package:news_today_app/Responses/category.dart';
import 'package:news_today_app/Widgets/Home/home_cont.dart';
import 'package:news_today_app/Widgets/Home/home_image.dart';

import 'article_view.dart';

class CategoriesScreen extends StatefulWidget {

  final String title;
  CategoriesScreen({this.title});

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {


  bool _isLoading = true;

  @override
  void initState() {
    getCategoryNews();
    super.initState();
  }

  getCategoryNews() async{
    CategoryNewsClass newsClass = CategoryNewsClass();
    await newsClass.getCategoriesNews(widget.title);
    news = newsClass.categories;
    setState(() {
      _isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        centerTitle: true,
        elevation: 0.0,
          leading: IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Theme.of(context).primaryColor,
            ),
          ),
          title: Text(
            widget.title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 22
            ),
          ),
      ),
      body: _isLoading? Center(child: CircularProgressIndicator(
        backgroundColor: Theme.of(context).primaryColor,
      )
      ) : ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          HomeImage(
            width: 390,
          ),
          SizedBox(
            height: 50,
          ),
          HomeCont(
            title: news,
            right: 250,
            left: 200,
            rr: 30,
          )
          // SingleChildScrollView(
          //   child: Container(
          //     child: Column(children: <Widget>[
          //       //            Blogs
          //       Container(
          //         child: ListView.builder(itemCount: news.length,
          //             shrinkWrap: true,
          //             scrollDirection: Axis.vertical,
          //             physics: ClampingScrollPhysics(),
          //             itemBuilder: (context , index) =>
          //                 InkWell(
          //                   onTap: (){
          //                     Navigator.push(context, MaterialPageRoute(builder: (context) =>ArticleView(
          //                       blogUrl: news[index].url,
          //                     )));
          //
          //                   },
          //                   child: Container(
          //                     padding: EdgeInsets.symmetric(vertical: 8,horizontal: 8),
          //                     margin: EdgeInsets.only(bottom: 15),
          //                     child: Column(
          //                       children: <Widget>[
          //                         ClipRRect(
          //                             borderRadius: BorderRadius.circular(5),
          //                             child: Image.network(news[index].image)),
          //                         SizedBox(height: 10,),
          //                         Text(news[index].title,style: TextStyle(color: Colors.black87,fontSize: 17,fontWeight: FontWeight.bold),),
          //                         SizedBox(height: 5,),
          //                         Text(news[index].desc,style: TextStyle(color: Colors.black54),),
          //                       ],
          //                     ),
          //                   ),
          //                 )
          //         ),
          //       )
          //
          //     ],),
          //   ),
          // ),
        ],
      ),
    );
  }
}
