import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news_today_app/Classes/news.dart';
class HotNews3{
  List<ArticleModel> news5 = [];

  Future<void> getHotNews() async{
    String url = 'http://newsapi.org/v2/top-headlines?country=it&category=business&apiKey=c6409c8598bf44398d2c8b59d2714f35';

    var response = await http.get(url);
    var jsonData  = json.decode(response.body);
    if(jsonData['status'] == 'ok'){
      jsonData['articles'].forEach((data){
        if(data['author'] != null && ['description'] != null){
          ArticleModel articleModel3 = ArticleModel(
              title: data['title'],
              desc: data['description'],
              auther: data['author'],
              url: data['url'],
              image: data['urlToImage'],
              content: data['content'],
              time: data["publishedAt"]
          );
          news5.add(articleModel3);
        }
      });
    }
  }
}