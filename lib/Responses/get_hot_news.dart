import 'dart:convert';

import 'package:news_today_app/Classes/news.dart';
import 'package:http/http.dart' as http;
class HotNews{
  List<ArticleModel> news3 = [];

  Future<void> getHotNews() async{
    String url = 'http://newsapi.org/v2/top-headlines?country=us&apiKey=c6409c8598bf44398d2c8b59d2714f35';

    var response = await http.get(url);
    var jsonData  = json.decode(response.body);
    if(jsonData['status'] == 'ok'){
      jsonData['articles'].forEach((data){
        if(data['author'] != null && ['description'] != null){
          ArticleModel articleModel2 = ArticleModel(
              title: data['title'],
              desc: data['description'],
              auther: data['author'],
              url: data['url'],
              image: data['urlToImage'],
              content: data['content'],
              time: data["publishedAt"]
          );
          news3.add(articleModel2);
        }
      });
    }
  }
}